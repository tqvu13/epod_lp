let mix = require('laravel-mix');
mix.setPublicPath('./dist');
mix.setResourceRoot('../');
mix.webpackConfig({
  module: {
    loaders: [],
    rules: [
      // Font files
      {
        test: /\.(eot|ttf|otf|woff|woff2)$/,
        loader: 'file-loader',
        query:{
          outputPath: './fonts/',
          name: '[name].[ext]'
        }
      },
    ],
  }
});

mix.sass('./resources/scss/style.scss', 'css/style.css')
  .js('./resources/js/app.js', 'js/app.js')
  .extract([
    'lodash',
    'jquery',
    'popper.js',
    'bootstrap',
    'fullpage.js/dist/fullpage.extensions.min',
  ])
  .sourceMaps();

mix.copyDirectory('./resources/images', 'dist/images/');