<?php

require_once('./config.php');

$formType = (isset($_POST['form_type'])) ? $_POST['form_type'] : 1;

if (
  empty($_POST['company']) || 
  empty($_POST['department']) || 
  empty($_POST['pic_name']) || 
  empty($_POST['phone']) ||
  empty($_POST['email']) ||
  !in_array($formType,[1,2]) ||
  ($formType == 1 ? empty($_POST['quantity']) : !isset($_POST['content']))
) {
    header('Location: ./user_form.html');
    exit();
}

try {
    $conn = new PDO("mysql:host={$config['servername']};dbname={$config['database']}", $config['username'], $config['password']);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn->exec("SET NAMES 'utf8'");
}
catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}

$posted = false;
$error = false;
$message = '';
$sql = '';
$stmt = "";

$data = [
    'company' => $_POST['company'],
    'department' => $_POST['department'],
    'surname' => $_POST['surname'],
    'pic_name' => $_POST['pic_name'],
    'phone' => $_POST['phone'],
    'email' => $_POST['email'],
];

if($formType == 1) {
  $data['quantity'] = intval($_POST['quantity']);
  $stmt = $conn->prepare("INSERT INTO lp_contact (company, department, surname, pic_name, phone, email, quantity) VALUES (:company, :department, :surname, :pic_name, :phone, :email, :quantity)");
  $stmt->bindValue(':quantity', $data['quantity']);
} else if($formType == 2) {
  $data['content'] = $_POST['content'];
  $stmt = $conn->prepare("INSERT INTO lp_contact2 (company, department, pic_name, phone, email, content) VALUES (:company, :department, :pic_name, :phone, :email, :content)");
  $stmt->bindValue(':content', $data['content']);
}

$stmt->bindValue(':company', $data['company']);
$stmt->bindValue(':department', $data['department']);
$stmt->bindValue(':surname', $data['surname']);
$stmt->bindValue(':pic_name', $data['pic_name']);
$stmt->bindValue(':phone', $data['phone']);
$stmt->bindValue(':email', $data['email']);

if ($stmt && $stmt->execute()) {
  $posted = true;
  $message = "New contact created successfully";
  header('Location: ./success.html');
} else {
  $posted = true;
  $error = true;
  $message = "Error:" . $conn->error;
}

$conn = null;
die();