require('./bootstrap');
import fullpage from 'fullpage.js/dist/fullpage.extensions.min';

function initAnimate(destination) {
  let animatedItems = $(destination.item).find('.ready-animate');
  
  if(animatedItems.length == 1) {
    $(animatedItems).addClass('animated bounceInUp').css('animation-delay', '.5s');
  } else {
    $(animatedItems).addClass('animated bounceInUp');
    
    $(animatedItems).each(function(index) {
      $(this).css('animation-delay', .25*(index + 1) + 's');
    });
  }
}

function removeAnimate(origin) {
  $(origin.item).find('.ready-animate').removeClass('animated bounceInUp');
}

$(window).on('load', function() { // makes sure the whole site is loaded 
  $('#status').fadeOut(); // will first fade out the loading animation 
  $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
  $('body').delay(350).css({'overflow':'visible'});
})

$(function() {
  var epodFullpage = new fullpage('#fullpage', {
    //options here
    licenseKey: "OPEN-SOURCE-GPLV3-LICENSE",
    autoScrolling: true,
    scrollHorizontally: true,
    paddingTop: '71px',
    fixedElements: '#topnav',
    afterRender: function(){
      var pluginContainer = this;
      $('.fp-controlArrow.fp-next').html('<div class="arrow" onclick="$(this).parent().click();"><div class="arrow-top"></div><div class="arrow-bottom"></div></div>');
      $('.fp-controlArrow.fp-prev').html('<div class="arrow" onclick="$(this).parent().click();"><div class="arrow-top"></div><div class="arrow-bottom"></div></div>');
    },
    afterLoad: function(origin, destination, direction) {
      initAnimate(destination);
    },
    onLeave: function(origin, destination, direction) {
      // removeAnimate(origin);
      initAnimate(destination);
    }
  });

  $('.section-footer').on('inview', function(event, isInView) {
    if(isInView) {
      $('.epod-arrow').hide();
    } else {
      $('.epod-arrow').show();
    }
  });

  $('.epod-arrow').off('click').on('click', function(e) {
    e.preventDefault();
    epodFullpage.moveSectionDown();
  });

});